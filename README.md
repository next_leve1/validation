## Simple and powerful

# Validation

## Install

```
yarn add @next_leve1/validation
```

## Usage

```js
import validation from '@next_leve1/validation'

const validateUser = validation.create({
  /*
    Any value validation or array of value validations will be validated in a row as a layer.
  */
  name: [string, [nameMinLength(2), nameMaxLength(50)], [nameTheSame, nameNotAvailable]],
  /*
    In the example above, first layer of validations is a "string" validation. In this layer it can return just
    "string" validation result if invalid.

    If the value is a string, then it runs next layer of validations: "nameMinLength(2)"and "nameMaxLength(50)".
    In this layer, it can return "nameMinLength(2)" or/and "nameMaxLength(50)" validation results if invalid.

    If the value more than 2 and less than 50 characters in the same time, then all ok with previous layer
    and it run next layer of validations: "nameTheSame" and "nameNotAvailable". In this layer, it can return
    "nameTheSame" or/and "nameNotAvailable" validation results if invalid.

    If all layers valid it returns nothing for this property.
  */
})

const validationResult = await validateUser({ name: 'name' }, { userId: 1 })
/*
  Note about it supports asynchronous validation. In example below, "nameTheSame" and "nameNotAvailable" are
  asynchronous validations. That's why it return a promise.

  If all values valid it returns null.
*/

console.log(validationResult)

/*
  {
    name: [
      'This name is the same',
      'This name is not available',
    ],
  }
*/
```

For more details look at [tests](https://gitlab.com/love_intent/validation/blob/master/test/index.test.js)
