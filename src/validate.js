export const resolveValidator = (validate) => async (value, context) => {
  if (!(validate[Symbol.toStringTag] === 'AsyncFunction')) {
    return validate(value, context)
  }

  return await validate(value, context)
}

export default (...validatorsGroups) => async (value, context) => {
  const validatorsResult = []

  for (const validatorsGroup of validatorsGroups) {
    if (Array.isArray(validatorsGroup)) {
      for (const validator of validatorsGroup) {
        const validateResult = await resolveValidator(validator)(value, context)

        if (validateResult) {
          validatorsResult.push(validateResult)
        }
      }

      if (validatorsResult.length > 0) {
        break
      }
    } else {
      const validator = validatorsGroup

      const validateResult = await resolveValidator(validator)(value, context)

      if (validateResult) {
        validatorsResult.push(validateResult)

        break
      }
    }
  }

  if (validatorsResult.length > 0) {
    return validatorsResult
  }

  return null
}
