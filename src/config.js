const DEVELOPMENT = 'development'
const TEST = 'test'
const PRODUCTION = 'production'

const mode = process.env.NODE_ENV

export default {
  mode,
  isProduction: mode === PRODUCTION,
  isDevelopment: mode === DEVELOPMENT,
  isTesting: mode === TEST,
}
