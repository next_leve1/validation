import validate from '@/validate'

export default async function validateObject(validatorsGroupsMap, map, context) {
  const validationResult = {}

  for (const [key, validatorsGroups] of Object.entries(validatorsGroupsMap)) {
    const { [key]: value } = map

    let validateResult
    if (Array.isArray(validatorsGroups)) {
      validateResult = await validate(...validatorsGroups)(value, context)
    } else {
      validateResult = await validateObject(validatorsGroups, map[key], context)
    }

    if (validateResult) {
      validationResult[key] = validateResult
    }
  }

  const isInvalid = Object.entries(validationResult).some(
    ([, validatorsResult]) => validatorsResult,
  )

  if (isInvalid) {
    return validationResult
  }

  return null
}
