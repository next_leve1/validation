import { assert } from 'chai'
import validation from '@/index'
import fake from './fake'

const messages = {
  string: 'Should be a string',
  required: 'Is required',
  minLength: (length) => `Should be at least ${length} characters long`,
  maxLength: (length) => `Should be less than or equal to ${length} characters long`,
  async1: 'Failed in async1',
  async2: 'Failed in async2',
}

const makeAsync1 = async (contextProp1, value) => contextProp1 === 'valid' && value
const makeAsync2 = async (contextProp1, value) => contextProp1 === 'valid' && value

export const required = (value) => !value && messages.required
export const minLength = (min) => (value) =>
  typeof value === 'string' && value.length < min && messages.minLength(min)
export const maxLength = (max) => (value) =>
  typeof value === 'string' && value.length > max && messages.maxLength(max)
export const async1 = async (value, { contextProp1 }) =>
  !(await makeAsync1(contextProp1, value)) && messages.async1
export const async2 = async (value, { contextProp1 }) =>
  !(await makeAsync2(contextProp1, value)) && messages.async2

describe('validation', () => {
  const validateData = validation.create({
    prop1: [maxLength(50), async1],
    prop2: [required, maxLength(50), [async1, async2]],
    prop3: {
      subProp1: [maxLength(80)],
    },
  })

  it('validate ok', async () => {
    const validationResult = await validateData(fake.data(), fake.context())

    assert.isNull(validationResult)
  })

  it('validate failed prop1 max length prop2 required subProp1 max length', async () => {
    const validationResult = await validateData({
      prop1: fake.prop1Max(),
      prop2: fake.prop2Required(),
      prop3: {
        subProp1: fake.subProp1Max(),
      },
    })

    assert.deepEqual(validationResult, {
      prop1: [messages.maxLength(50)],
      prop2: [messages.required],
      prop3: {
        subProp1: [messages.maxLength(80)],
      },
    })
  })

  it('validate failed prop1 async1 and prop2 async1 and async2', async () => {
    const validationResult = await validateData(fake.data(), {
      contextProp1: fake.contextProp1Invalid(),
    })

    assert.deepEqual(validationResult, {
      prop1: [messages.async1],
      prop2: [messages.async1, messages.async2],
    })
  })
})
