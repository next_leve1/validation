import faker from 'faker'

export const prop1 = () => faker.lorem.word()
export const prop2 = () => faker.lorem.word()
export const subProp1 = () => faker.lorem.word()
export const prop3 = () => ({
  subProp1: subProp1(),
})
export const data = () => ({
  prop1: prop1(),
  prop2: prop2(),
  prop3: prop3(),
})
export const contextProp1 = () => 'valid'
export const context = () => ({
  contextProp1: contextProp1(),
})
export const prop1Max = () => faker.lorem.paragraphs()
export const prop2Required = () => ''
export const subProp1Max = () => faker.lorem.paragraphs()
export const contextProp1Invalid = () => 'invalid'

export default {
  prop1,
  prop2,
  subProp1,
  prop3,
  data,
  contextProp1,
  context,
  prop1Max,
  prop2Required,
  subProp1Max,
  contextProp1Invalid,
}
