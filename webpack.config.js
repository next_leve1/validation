import path from 'path'
import webpack from 'webpack'
import config from '@/config'

const plugins = []

if (config.isDevelopment) {
}

export default {
  target: 'node',
  context: __dirname,
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'index.js',
    libraryTarget: 'umd',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
    ],
  },
  plugins,
  mode: config.isProduction ? 'production' : 'development',
}
