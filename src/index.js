import validate from '@/validate'
import create from '@/create'

export default {
  validate,
  create,
}
