import validateObject from '@/validate-object'

export default (validatorsGroupsMap) => (map, context) =>
  validateObject(validatorsGroupsMap, map, context)
